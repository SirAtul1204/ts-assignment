enum ESex {
  male = "Male",
  female = "Female",
  other = "Other",
}

type TSex = ESex.male | ESex.female | ESex.other;
type TField = "name" | "age" | "salary" | "sex";
type TOrder = "ASCE" | "DESC";

class Person {
  name: string;
  age: number;
  salary: number;
  sex: TSex;

  constructor(name: string, age: number, salary: number, sex: TSex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }

  static sort(persons: Person[], field: TField, order: TOrder): Person[] {
    let arr = [...persons];

    function swap(a: number, b: number) {
      [arr[a], arr[b]] = [arr[b], arr[a]];
    }

    function partition(low: number, high: number): number {
      let pivot = arr[high];
      let i = low - 1;

      for (let j = low; j <= high - 1; j++) {
        if (arr[j][`${field}`] < pivot[`${field}`]) {
          i++;
          swap(i, j);
        }
      }

      swap(i + 1, high);
      return i + 1;
    }

    function quickSort(low: number, high: number) {
      if (low < high) {
        const pi = partition(low, high);

        quickSort(low, pi - 1);
        quickSort(pi + 1, high);
      }
    }

    quickSort(0, arr.length - 1);

    return order === "DESC" ? arr.reverse() : arr;
  }
}

const persons = [
  new Person("Atul", 21, 20000, ESex.male),
  new Person("Aditi", 23, 40000, ESex.female),
  new Person("Ayush", 18, 10000, ESex.male),
  new Person("Hoshali", 15, 25000, ESex.female),
  new Person("Nishant", 34, 7800, ESex.male),
  new Person("Apurv", 39, 50000, ESex.male),
];

const sortedArr = Person.sort(persons, "name", "DESC");

console.log(sortedArr);
